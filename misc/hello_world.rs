fn add(x: i64, y: i64) -> i64{
	x+y
}

fn main() {
	let seq = range(1i64, 10i64).fold(0i64, add);
	println!("Hello, world!");
}
