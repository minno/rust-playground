use std::io::{File, BufferedReader};

// Returns a list of the numbers in the input file, each represented
// as a list of decimal digits, lsd to msd
fn read_nums() -> Vec<Vec<uint>> {
    let mut file = BufferedReader::new(File::open(&Path::new("input13.txt")));
    fn line_to_digits(line: &String) -> Option<Vec<uint>> {
        let digits: Vec<uint> = line.as_slice().trim()
            .as_bytes().iter().rev()
            .map(|&x| { (x - '0' as u8) as uint }).collect();
        match digits.len() {
            0 => None,
            _ => Some(digits),
        }
    }

    file.lines().filter_map(|line| { line_to_digits(&line.unwrap()) }).collect()
}

// Adds up each digit's place. Does not carry, so digits will end up
// being more than 9
fn add_cols(nums: &Vec<Vec<uint>>) -> Vec<uint> {
    let num_len = nums[0].len();
    let mut sums = Vec::from_elem(num_len, 0u);
    for num in nums.iter() {
        for i in range(0, num_len) {
            *sums.get_mut(i) += num[i];
        }
    }
    sums
}

// Carries between digits, bringing each digit down to the range [0, 9].
// Appends extra digits if the msd also needs to be carried from
fn do_carries(nums: &mut Vec<uint>) {
    let mut carry = 0;
    for digit in nums.mut_iter() {
        *digit += carry;
        carry = *digit / 10;
        *digit %= 10;
    }
    while carry > 0 {
        nums.push(carry % 10);
        carry /= 10;
    }
}

fn main() {
    let nums = read_nums();
    let mut digits = add_cols(&nums);
    do_carries(&mut digits);
    for digit in digits.iter().rev() {
        print!("{}", digit);
    }
    println!("");
}

