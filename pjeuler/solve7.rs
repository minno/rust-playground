#![feature(globs)]
extern crate debug;
use std::vec::*;
use std::iter::*;

fn primes_count(p_count: uint) -> uint {
	let block_size = 256; // Completely arbitrary
	let mut primes: Vec<uint> = vec!(2, 3, 5, 7, 11, 13, 17, 19);
	for block_index in count(0u, 1u) {
		let mut block: Vec<bool> = Vec::from_elem(block_size, true);
		let block_begin = block_index * block_size;
		let block_end = block_begin + block_size;
		for prime_index in count(0u, 1u) {
			let curr_prime = primes[prime_index];
			if curr_prime*curr_prime > block_end {
				break;
			}
			// begin is the index of the first number in the block
			// [block_begin, block_end) that is divisible by curr_prime
			let begin = if curr_prime*curr_prime > block_begin {
					curr_prime * curr_prime
			} else {
					((block_begin + (curr_prime-1)) / curr_prime) *
					curr_prime
			};
			for i in range_step(begin-block_begin, block_size, curr_prime) {
				*block.get_mut(i) = false;
			}
		}
		for num in range(0, block_size) {
			if block[num] && num+block_begin > primes[primes.len()-1] {
				primes.push(num+block_begin);
			}
		}

		if primes.len() > p_count-1 {
			return primes[p_count-1];
		}
	}
	return 0;
}

fn main() {
	let result = primes_count(10001);
	println!("{}", result);
}




