use std::iter::{count, range_step};

// Returns a list of the primes in the range [2, limit)
pub fn sieve_to(limit: uint) -> Vec<uint> {
	if limit < 2 {
		return Vec::new();
	}
	let mut block = Vec::from_elem(limit, true);
	*block.get_mut(0) = false;
	*block.get_mut(1) = false;
	for prime in count(2u, 1u).take_while(|&i| { i*i < limit }) {
		if !*block.get_mut(prime) {
			continue;
		}
		for i in range_step(prime*prime, limit, prime) {
			*block.get_mut(i) = false;
		}
	}
	let primes = count(0u, 1u).zip(block.iter())
		.filter(|&(_, is_prime)| *is_prime).map(|(num, &_)| num).collect();
	primes
}
