use std::collections::treemap::TreeMap;

fn collatz(n: uint) -> uint {
    match n%2 {
        0 => n/2,
        _ => 3*n + 1,
    }
}

fn longest_collatz_seq<Seq: Iterator<uint>>(seq: &mut Seq) -> uint {
    // This would be so much nicer if I could have a static local
    // memo, so I could just have a collatz_len() function
    
    let mut memo: TreeMap<uint, uint> = TreeMap::new();
    memo.insert(1, 1);

    fn collatz_len(n: uint, memo: &mut TreeMap<uint, uint>) -> uint {
        let mut counter = 0;
        let mut n = n;

        // After the algorithm finishes, it backfills the memo entries
        // that weren't there when it could have used them
        let mut path = Vec::new();
        
        loop {
            match memo.find(&n) {
                Some(&len) => {
                    counter += len;
                    break;
                }
                None => {}
            }

            path.push(n);
            n = collatz(n);
            counter += 1;
        }

        for (i, num) in range(counter-path.len()+1, counter+1)
                        .zip(path.move_iter().rev()) {
            memo.insert(num, i);
        }
        counter
    }

    let mut max_len = 0;
    let mut max_start = 0;
    for val in *seq {
        let len = collatz_len(val, &mut memo);
        if len > max_len {
            max_len = len;
            max_start = val;
        }
    }
    max_start
}


fn main() {
    let biggest = longest_collatz_seq(&mut range(1u, 1_000_000));
    println!("{}", biggest);

}
