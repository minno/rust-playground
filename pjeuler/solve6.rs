use std::iter::Iterator;

fn square_of_sum<Seq: Iterator<i64> + Clone>(seq: Seq) -> i64 {
	let sum = seq.clone().fold(0i64, |a, b| {a + b});
	sum*sum
}

fn sum_of_squares<Seq: Iterator<i64> + Clone>(seq: Seq) -> i64 {
	seq.clone().map(|x| {x*x}).fold(0i64, |a, b| {a+b})
}

fn main() {
	let nums = std::iter::range_inclusive(0i64, 100i64);
	let diff = square_of_sum(nums.clone()) -
			sum_of_squares(nums.clone());
	println!("{}", diff);
}
	
