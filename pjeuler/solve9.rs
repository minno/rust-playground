fn is_triple(a: int, b: int, c: int) -> bool {
	a*a + b*b == c*c
}

fn main() {
	for a in range(1, 1000) {
		for b in range(1, a) {
			if is_triple(a, b, 1000-a-b) {
				println!("{}", a*b*(1000-a-b));
			}
		}
	}
}
