extern crate debug;
use std::io::File;
use std::collections::ringbuf::RingBuf;
use std::collections::Deque;

static RUN_LEN: uint = 13;

fn read_data() -> Vec<uint> {
	let filename = Path::new("input8.txt");
	let mut f = match File::open(&filename) {
		Ok(f) => f,
		Err(e) => fail!("Couldn't open file: {}", e),
	};
	let data = match f.read_to_end() {
		Ok(d) => d,
		Err(e) => fail!("Couldn't read from file: {}", e),
	};
	let is_digit = |c: &u8| { *c >= '0' as u8 && *c <= '9' as u8 };
	data.move_iter().filter(is_digit).map(|c| {(c - '0' as u8) as uint}).collect()
}

fn max_prod_string(data: &Vec<uint>) -> uint {
	if data.len() < RUN_LEN {
		fail!("Data too short: needed {} elements, got {}", RUN_LEN, data.len());
	}
	let mut max_prod = 0u;
	let mut window = RingBuf::<uint>::with_capacity(RUN_LEN);
	let seq_prod = |seq: &RingBuf<uint>| { seq.iter().fold(1u, |a,&b| a*b) };
	window.extend(data.iter().take(RUN_LEN).map(|&x| x)); // Because Iterator<&uint> != Iterator<uint>...
	for el in data.iter().skip(RUN_LEN) {
		let prod = seq_prod(&window);
		if prod > max_prod {
			max_prod = prod;
		}
		window.pop_front();
		window.push(*el);
	}
	if seq_prod(&window) > max_prod {
		max_prod = seq_prod(&window);
	}
	max_prod
}

fn main() {
	let data = read_data();
	let result = max_prod_string(&data);
	println!("{}", result);
}
