use std::iter::AdditiveIterator;
mod primes;

fn main() {
	let primes = primes::sieve_to(2_000_00000);
	println!("{}", primes.move_iter().sum());
}
