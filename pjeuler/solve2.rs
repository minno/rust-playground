#![feature(globs)]

use std::iter::*;
use std::default::Default;

struct FibStream {
	a: i64,
	b: i64
}

impl Default for FibStream {
	fn default () -> FibStream {
		FibStream {a: 0, b: 1}
	}
}

impl Iterator<i64> for FibStream {
	fn next(&mut self) -> Option<i64> {
		let old_b = self.b;
		self.b += self.a;
		self.a = old_b;
		Some(self.a)
	}
}

fn main() {
	let nums = FibStream { ..Default::default() };
	let sum = nums.take_while(|&n| {n < 4000000})
			.filter(|n| {n%2 == 0}).sum();
	println!("{}", sum);
}		
