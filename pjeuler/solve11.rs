use std::io::{BufferedReader, File};
use std::sync::Arc;

fn read_grid() -> Vec<Vec<uint>> {
	let path = Path::new("input11.txt");
	let mut file = BufferedReader::new(File::open(&path));
	let mut grid: Vec<Vec<uint>> = Vec::new();
	for line in file.lines() {
		let line = line.unwrap();
		let line_str = line.as_slice();
		let mut nums = Vec::new();
		for el in line_str.words() {
			match from_str::<uint>(el) {
				Some(num) => nums.push(num),
				None      => {}
			}
		}
		grid.push(nums);
	}
	grid
}

fn get_seq_products(grid: &Vec<Vec<uint>>,
					seq_length: uint,
					incr: (uint, uint)) -> uint {
	let (dx, dy) = incr;
	let mut max_prod = 0;
	let x_lim = grid.len();
	let y_lim = match x_lim {
		0 => 0,
		_ => grid[0].len(),
	};
	for x in range(0u, x_lim) {
		'el_loop: for y in range(0u, y_lim) {
			let mut acc = 1;
			for step_amount in range(0u, seq_length) {
				let new_x = x + dx*step_amount;
				let new_y = y + dy*step_amount;
				if new_x < x_lim && new_y < y_lim {
					acc *= grid[new_x][new_y];
				} else {
					break 'el_loop;
				}
			}
			if acc > max_prod {
				max_prod = acc;
			}
		}
	}
	max_prod
}
		
fn main() {
	let grid = Arc::new(read_grid());
	let (tx, rx) = channel();
	for incr_amnt in [(1, 0), (1, 1), (0, 1), (-1 as uint, 1)].iter() {
		let grid = grid.clone();
		let tx = tx.clone();
		let incr_amnt = *incr_amnt;
		spawn(proc() {
			let max_prod = get_seq_products(&*grid, 4, incr_amnt);
			tx.send(max_prod);
		});
	}
	let mut max_prod = 0;
	for _ in range(0i, 4) {
		let new_prod = rx.recv();
		if new_prod > max_prod {
			max_prod = new_prod;
		}
	}
	println!("{}", max_prod);
}
