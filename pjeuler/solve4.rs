#![feature(globs)]

use std::iter::*;

fn is_palindrome(val: uint) -> bool {
	let mut acc = 0;
	let mut n = val;
	while n > 0 {
		acc *= 10;
		acc += n % 10;
		n /= 10;
	}
	val == acc
}

fn main() {
	let mut pairs = range_inclusive(100u, 999)
			.map(|n| { Repeat::new(n).zip(range_inclusive(n, 999))});
	let mut max_prod = 0;
	let mut num1 = 0;
	let mut num2 = 0;
	for mut line in pairs {
		for (n1, n2) in line {
			if is_palindrome(n1*n2) && n1*n2 > max_prod {
				max_prod = n1*n2;
				num1 = n1;
				num2 = n2;
			}
		}
	}
	println!("{}:{}:{}", num1, num2, max_prod);
}


	
