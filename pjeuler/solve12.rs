
// Counts the multiplicity of every prime factor of the number,
// and returns a vector with those counts
fn factor_multiplicities(num: uint) -> Vec<uint> {
	let mut num = num;
	let mut result = Vec::new();
	let mut factor = 2;
	while num > 1 {
		let mut curr_mult = 0;
		while num % factor == 0 {
			curr_mult += 1;
			num /= factor;
		}
		if curr_mult > 0 {
			result.push(curr_mult);
		}
		factor += 1;
	}
	result
}

fn num_factors(num: uint) -> uint {
	let mults = factor_multiplicities(num);
	mults.iter().fold(1, |a,&b| { a*(b+1) })
}

struct TriangularNumbers {
	counter: uint,
	sum: uint
}

impl Iterator<uint> for TriangularNumbers {
	fn next(&mut self) -> Option<uint> {
		self.counter += 1;
		self.sum += self.counter;
		Some(self.sum)
	}
}

fn triangular_numbers() -> TriangularNumbers {
	TriangularNumbers{counter: 0, sum: 0}
}

fn main() {
	for num in triangular_numbers() {
		if num_factors(num) > 500 {
			println!("{}", num);
			break;
		}
	}
}

