
fn largest_factor(mut n: i64) -> i64 {
	let mut f = 1i64;
	while n > 1 {
		f += 1;
		while n % f == 0 {
			n /= f;
		}
		if f * f > n {
			return n;
		}
	}
	1
}



fn main() {
	let num = 600851475143i64;
	println!("{}", largest_factor(num));
}

