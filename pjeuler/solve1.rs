use std::iter::AdditiveIterator;

fn main() {
		let range = std::iter::range(0u, 1000);
		let mut nums = range.filter(|n| { n%3 == 0 || n%5 == 0 });
		let sum = nums.sum();
		println!("{}", sum);
}
