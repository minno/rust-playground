use std::iter::Iterator;


fn gcd(a: i64, b: i64) -> i64 {
	
	fn helper(a: i64, b: i64) -> i64 {
		if b == 0 {
			return a;
		}
		return helper(b, a%b);
	}

	if a > b {
		helper(a, b)
	} else {
		helper(b, a)
	}
}

fn lcm(a: i64, b: i64) -> i64 {
	a * (b / gcd(a, b))
}

fn main() {
	let result = std::iter::range_inclusive(1i64, 20i64).fold(1i64, lcm);
	println!("{}", result);
}	
